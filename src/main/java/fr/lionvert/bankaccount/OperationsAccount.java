package fr.lionvert.bankaccount;

public interface OperationsAccount {

	Account deposit(Long amount);
	
	Account withdrawal(Long amount);
	
	String displayOperationsHistory();
}
