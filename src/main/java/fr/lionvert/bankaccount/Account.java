package fr.lionvert.bankaccount;

import java.util.Date;
import java.util.Optional;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Builder
@Getter
@Data
@AllArgsConstructor
public class Account implements OperationsAccount{
	private final Long idAccount;
	private final Long amountAccount;
	private final Set<Operation> operations;

	@Override
	public Account deposit(Long amount) {
		return Optional.ofNullable(amount)
				.map(this::doDeposit)
				.orElse(this);
				
	}
	
	private Account doDeposit(Long amount) {
		return Account.builder()
				.idAccount(idAccount)
				.amountAccount(amountAccount+amount)
				.operations(operations)
				.build()
				.addOperationHistory("Deposit", amount);
	}

	@Override
	public Account withdrawal(Long amount) {
		return Optional.ofNullable(amount)
				.filter(this::isValid)
				.map(this::doWithdrawal)
				.orElse(this);
	}
	
	private Account doWithdrawal(Long amount) {
		return Account.builder()
				.idAccount(idAccount)
				.amountAccount(amountAccount-amount)
				.operations(operations)
				.build()
				.addOperationHistory("Withdrawal", amount);
	}
	
	private boolean isValid(Long amount) {
		return getAmountAccount() >= amount;
	}
	
	private Account addOperationHistory(String operationName, Long amount) {
		Operation operation = Operation.builder()
				.amountOperation(amount)
				.balance(amountAccount)
				.dateOperation(new Date())
				.nameOperation(operationName)
				.build();
		operations.add(operation);
		return Account.builder()
				.idAccount(idAccount)
				.amountAccount(amountAccount)
				.operations(operations)
				.build();
	}

	@Override
	public String displayOperationsHistory() {
		String history = "";
		for(Operation operation: operations) {
			history += operation.toString();
		}
		return history;
	}
	
}
