package fr.lion.bankaccount;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.lionvert.bankaccount.Account;
import fr.lionvert.bankaccount.Operation;

public class DepositAccountTest {
	
	static private Map<Long, Account> accountsSet = new HashMap<>();
	
	@BeforeAll
	static void createAccount() {
		Account account1 = Account.builder().idAccount(1234567L).amountAccount(10L).operations(new HashSet<Operation>()).build();
		Account account2 = Account.builder().idAccount(12345895L).amountAccount(100L).operations(new HashSet<Operation>()).build();
		Account account3 = Account.builder().idAccount(85698745L).amountAccount(-10L).operations(new HashSet<Operation>()).build();
		accountsSet.put(account1.getIdAccount(), account1);
		accountsSet.put(account2.getIdAccount(), account2);
		accountsSet.put(account3.getIdAccount(), account3);
	}
	
	@Test
	void testValidAmountDeposit() {
		Account account = accountsSet.get(1234567L);
		Account accountAfterDeposit = account.deposit(10L);
		Assertions.assertThat(accountAfterDeposit.getIdAccount()).isEqualTo(1234567L);
		Assertions.assertThat(accountAfterDeposit.getAmountAccount()).isEqualTo(20);
	}
	
	@Test
	void testInvalidAmountDeposit() {
		Account account = accountsSet.get(12345895L);
		Account accountAfterDeposit = account.deposit(null);
		Assertions.assertThat(accountAfterDeposit.getAmountAccount()).isEqualTo(100L);
	}

}
