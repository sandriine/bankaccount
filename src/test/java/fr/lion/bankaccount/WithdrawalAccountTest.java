package fr.lion.bankaccount;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.lionvert.bankaccount.Account;
import fr.lionvert.bankaccount.Operation;

public class WithdrawalAccountTest {
	
	static private Map<Long, Account> accountsSet = new HashMap<>();
	
	@BeforeAll
	static void createAccount() {
		Account account1 = Account.builder().idAccount(1234567L).amountAccount(10L).operations(new HashSet<Operation>()).build();
		Account account2 = Account.builder().idAccount(12345895L).amountAccount(100L).operations(new HashSet<Operation>()).build();
		Account account3 = Account.builder().idAccount(85698745L).amountAccount(-10L).operations(new HashSet<Operation>()).build();
		accountsSet.put(account1.getIdAccount(), account1);
		accountsSet.put(account2.getIdAccount(), account2);
		accountsSet.put(account3.getIdAccount(), account3);
	}
	
	@Test
	void testValidAmountWithdrawal() {
		Account account = accountsSet.get(1234567L);
		Account accountAfterWithdrawal = account.withdrawal(8L);
		Assertions.assertThat(accountAfterWithdrawal.getIdAccount()).isEqualTo(1234567L);
		Assertions.assertThat(accountAfterWithdrawal.getAmountAccount()).isEqualTo(2L);
	}
	
	@Test
	void testValidAmountWithdrawalWithAmountAccountNull() {
		Account account = accountsSet.get(1234567L);
		Account accountAfterWithdrawal = account.withdrawal(10L);
		Assertions.assertThat(accountAfterWithdrawal.getIdAccount()).isEqualTo(1234567L);
		Assertions.assertThat(accountAfterWithdrawal.getAmountAccount()).isEqualTo(0L);
	}
	
	@Test
	void testValidAmountWithdrawalGreaterThanAmountAccount() {
		Account account = accountsSet.get(12345895L);
		Account accountAfterWithdrawal = account.withdrawal(120L);
		Assertions.assertThat(accountAfterWithdrawal.getAmountAccount()).isEqualTo(100L);
	}
	
	@Test
	void testInvalidAmountWithdrawalWithNegativeAmountAccount() {
		Account account = accountsSet.get(85698745L);
		Account accountAfterWithdrawal = account.withdrawal(10L);
		Assertions.assertThat(accountAfterWithdrawal.getAmountAccount()).isEqualTo(-10L);
	}
	
	@Test
	void testInvalidAmountWithdrawal(){
		Account account = accountsSet.get(1234567L);
		Account accountAfterWithdrawal = account.withdrawal(null);
		Assertions.assertThat(accountAfterWithdrawal.getIdAccount()).isEqualTo(1234567L);
		Assertions.assertThat(accountAfterWithdrawal.getAmountAccount()).isEqualTo(10L);
	}

}
