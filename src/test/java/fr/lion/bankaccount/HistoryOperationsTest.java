package fr.lion.bankaccount;

import java.util.HashSet;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import fr.lionvert.bankaccount.Account;
import fr.lionvert.bankaccount.Operation;

public class HistoryOperationsTest {
	
	@Test
	void displayOperationsHistory() {
		Account account = Account.builder().idAccount(1234567L).amountAccount(100L).operations(new HashSet<Operation>()).build();
		Account accountAfterOperations = account.deposit(10L).withdrawal(50L);
		String history = accountAfterOperations.displayOperationsHistory();
		Assertions.assertThat(accountAfterOperations.getOperations().size()).isEqualTo(2);
		Assertions.assertThat(accountAfterOperations.getOperations()).hasSize(2);
		Assertions.assertThat(history).hasLineCount(2);
	}

}
